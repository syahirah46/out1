<?php

use Illuminate\Http\Request;
use App\Role;
use App\Jawatan;
use App\Bahagian;
use App\Seksyen;
use App\User;
use App\Permohonan;
use App\Gantian;
use App\User_Role;
use App\User_Perjawatan;
use App\TujuanOuting;
use App\StatusPermohonan;
use App\Bahagian_Seksyen;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('api.basic')->group( function() {

// Route::get('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/myuser', function (Request $request) {
    return \Auth::User();
});

Route::get('/test', function () {
    return 'hello';
});
Route::get('/bahagianseksyen', 'Bahagian_SeksyenController@getApiBahagian_Seksyen');

//dashboard pengguna
Route::get('/penggunastatus/{user_id}/{status_permohonan_id}', 'PermohonanController@getdashboardpengguna');//dashboard pengguna ikut id dia dan status
Route::get('/totalmohon/{user_id}/{status_permohonan_id}', 'PermohonanController@totalpermohonan');//kira permohonan by status oleh pengguna
Route::get('/penggunamohon/{permohonan_id}', 'PermohonanController@getpermohonanuser');//view satu permohonan
Route::put('/penggunaupdate/{permohonan_id}', 'PermohonanController@updatepermohonan');//update satu permohonan oleh pengguna


//profil pengguna
Route::get('/user/{user_id}', 'UserController@getApiUser');
//Route::get('/usercreate', 'UserController@getCreateUser');
//Route::post('/usercreate', 'UserController@postCreateUser');


//permohonanuser
Route::put('/mohoncreate/{user_id}', 'permohonanController@create');
Route::get('/tujuan', 'TujuanOutingController@getApiTujuan');
Route::get('/gantian', 'GantianController@getApiGantian');
Route::get('/pelulus/{bahagian_id}', 'User_RoleController@userpelulus');//viewpelulus
Route::get('/penyokong/{bahagian_id}', 'User_RoleController@userpenyokong');//viewpenyokong

//dashboardpegawai
Route::get('/pegawaistatus/{seksyen_id}/{status_permohonan_id}', 'PermohonanController@dashboardpegawai');//dashboard pegawai ikut seksyen dan status
Route::get('/totalseksyen/{seksyen_id}/{status_permohonan_id}', 'PermohonanController@totalseksyen');//kira permohonan by status oleh pengguna
Route::put('/pegawailulus/{permohonan_id}', 'PermohonanController@pegawailulus');// pegawai meluluskan
Route::put('/pegawaitaklulus/{permohonan_id}', 'PermohonanController@pegawaitaklulus');// pegawai tak meluluskan


//admin
Route::get('/user', 'UserController@getAlluser');
Route::put('/activeuser/{user_id}', 'UserController@activeuser');//activeuser
Route::put('/deactiveuser/{user_id}', 'UserController@deactiveuser');//deactive user
Route::get('/usercreate', 'UserController@create');
Route::put('/userupdate/{user_id}', 'UserController@update');



// Route::get('/role', 'roleController@getApiRole');
Route::get('/bahagian', 'bahagianController@getApiBahagian');
Route::get('/bahagian/{id}', function ($id){
    return \App\Bahagian::find($id);
});
Route::get('/bahagian/{id}', 'BahagianController@create');
Route::put('/bahagianupdate/{id}', 'BahagianController@update');


//tarikh
Route::get('/tarikhbtw', 'PermohonanController@getbetween');


//jawatan
Route::get('/jawatan', 'jawatanController@getApiJawatan');
Route::get('/jawatan/{id}', function ($id){
    return \App\Jawatan::find($id);
});

//permohonan
Route::get('/permohonan', 'permohonanController@getApiPermohonan');
Route::get('/totalmohon/{id}/{status}', 'permohonanController@totalpermohonan');//kira permohonan by status oleh pengguna
Route::get('/totalseksyen/{seksyen_id}/{status}', 'permohonanController@totalseksyen');//kira permohonan by status oleh pengguna
Route::get('/permohonan/{id}', function ($id){
    return \App\Permohonan::find($id);
});
Route::get('/mohoncreate/{user_id}', 'permohonanController@create');
Route::get('/penggunastatus/{user_id}/{status}', 'permohonanController@getdashboardpengguna');//dashboard pengguna ikut id dia dan status

Route::get('/permohonanbatal/{id}', 'permohonanController@permohonanbatal');// pengguna batalkan permohonan

Route::get('/penggunamohon/{id}', 'permohonanController@getpermohonanuser');//view satu permohonan




//seksyen
Route::get('/seksyen', 'seksyenController@getApiSeksyen');
Route::get('/seksyenpegawai/{bahagian_id}', 'seksyenController@bahagianpegawai');//papar seksyen bahagian dia
Route::get('/seksyen/{id}', function ($id){
    return \App\Seksyen::find($id);
});



// });
});


//token
Route::post('/login', 'UserController@login_api');


