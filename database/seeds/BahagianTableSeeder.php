<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BahagianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
         //
         DB::table('Bahagian')->truncate();
         DB::table('Bahagian')->insert(
         [
             [
                 'nama' => 'Bahagian Pengurusan Maklumat',
                //  'seksyen_id' => '1',
                 'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                 'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
             ],
             [
                'nama' => 'Bahagian Khidmat Pengurusan',
                // 'seksyen_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'nama' => 'Bahagian Pengurusan Korporat',
                // 'seksyen_id' => '3',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
             ]);
     
         }
    }

