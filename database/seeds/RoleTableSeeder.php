<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //
         DB::table('Role')->truncate();
         DB::table('Role')->insert(
         [
             [
                 'nama' => 'Pengguna',
                //  'seksyen_id' => '1',
                 'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                 'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
             ],
             [
                'nama' => 'Penyokong',
                // 'seksyen_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'nama' => 'Pelulus',
                // 'seksyen_id' => '3',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            [
                'nama' => 'Pentadbir',
                // 'seksyen_id' => '3',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
             ]);
     
         }
    }

