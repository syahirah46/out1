<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class Bahagian_SeksyenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
         //
         DB::table('Bahagian_Seksyen')->truncate();
         DB::table('Bahagian_Seksyen')->insert(
         [
             [
                 'bahagian_id' => '1',
                 'seksyen_id' => '1',
                 'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                 'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
             ],
             [
                'bahagian_id' => '1',
                'seksyen_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'bahagian_id' => '1',
                'seksyen_id' => '3',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'bahagian_id' => '1',
                'seksyen_id' => '4',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'bahagian_id' => '1',
                'seksyen_id' => '5',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'bahagian_id' => '2',
                'seksyen_id' => '6',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'bahagian_id' => '3',
                'seksyen_id' => '7',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
             ]);
     
         }
    }

