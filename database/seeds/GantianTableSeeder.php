<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class GantianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //
         DB::table('Gantian')->truncate();
         DB::table('Gantian')->insert(
         [
             [
                 'nama' => 'Telah Digantikan',
                //  'seksyen_id' => '1',
                 'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                 'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
             ],
             [
                'nama' => 'Akan digantikan',
                // 'seksyen_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], 
             ]);
     
         }
    }

