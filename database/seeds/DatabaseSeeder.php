<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Bahagian_SeksyenTableSeeder::class);
        $this->call(JawatanTableSeeder::class);
        $this->call(GantianTableSeeder::class);
        $this->call(PermohonanTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(BahagianTableSeeder::class);
        $this->call(StatusPermohonanTableSeeder::class);
        $this->call(SeksyenTableSeeder::class);
        $this->call(TujuanOutingTableSeeder::class);
        $this->call(User_PerjawatanTableSeeder::class);
        $this->call(User_RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);


    }
}
