<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SeksyenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //
         DB::table('Seksyen')->truncate();
         DB::table('Seksyen')->insert(
         [
             [
                 'nama' => 'Seksyen aplikasi 1',
                //  'seksyen_id' => '1',
                 'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                 'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
             ],
             [
                'nama' => 'Seksyen aplikasi 2',
                // 'seksyen_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ], [
                'nama' => 'Seksyen PEngurusan Fasiliti dan portal',
                // 'seksyen_id' => '3',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            [
                'nama' => 'Seksyen Perancangan',
                // 'seksyen_id' => '3',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
             ]);
     
         }
    }

