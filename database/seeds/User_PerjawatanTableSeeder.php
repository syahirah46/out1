<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class User_PerjawatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //
         DB::table('User_Perjawatan')->truncate();
         DB::table('User_Perjawatan')->insert(
         [
             [
                'user_id'=> '2',
                'jawatan_id'=> '4',
                'bhgn_sek_id'=> '1',
                'created_at'=> '2019-09-07 10:16:38',
                'updated_at'=> '2019-09-07 10:16:38'
             ],
             [
                'user_id'=> '2',
                'jawatan_id'=> '4',
                'bhgn_sek_id'=> '1',
                'created_at'=> '2019-09-07 10:16:38',
                'updated_at'=> '2019-09-07 10:16:38'
            ], [
                'user_id'=> '2',
                'jawatan_id'=> '4',
                'bhgn_sek_id'=> '1',
                'created_at'=> '2019-09-07 10:16:38',
                'updated_at'=> '2019-09-07 10:16:38'
            ],
           
             ]);
     
         }
    }
