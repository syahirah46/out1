<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class JawatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  //
        DB::table('Jawatan')->truncate();
        DB::table('Jawatan')->insert(
        [
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'JUSA C',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'F 52',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'F48',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'F44',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'F41',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'FA38',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'FA32',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'FA29',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pegawai Teknologi Maklumat',
                'gred' => 'FA29 (KUP)',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Juruteknik Komputer',
                'gred' => 'FT22',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Juruteknik Komputer',
                'gred' => 'FT19',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pembantu Tadbir (KS)',
                'gred' => 'N22',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pembantu Tadbir Kewangan',
                'gred' => 'W19',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pembantu Tadbir',
                'gred' => 'N19',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pembantu Tadbir Perkeranian/Operasi',
                'gred' => 'N22',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pembantu Tadbir Perkranian / operasi',
                'gred' => 'N19',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'nama' => 'Pembantu Operasi',
                'gred' => 'N11',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            

            
            
            
            ]);
    
        }
   }

