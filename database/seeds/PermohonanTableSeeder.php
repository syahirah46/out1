<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermohonanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         //
         DB::table('Permohonan')->truncate();
         DB::table('Permohonan')->insert(
         [
             [
                 'user_id' => '1',
                 'tarikhkeluar' => '2019-09-05',
                 'masamulakeluar' => '00:00:00',
                 'masatamatkeluar' => '00:00:00',
                 'tujuan_id' => '1',
                 'lainlain' => '',
                 'gantian_id' => '1',
                 'tarikhgantian' => '2019-09-11',
                 'masamulagantian' => '00:00:00',
                 'masatamatgantian' => '00:00:00',
                 'status_permohonan_id' => '2',
                 'sebab_ditolak' => '',
                 'penyokong_user_role_id' => '1',
                 'pelulus_user_role_id' => '2',
                 'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                 'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
             ],
             
             [
                'user_id' => '1',
                'tarikhkeluar' => '2019-09-05',
                'masamulakeluar' => '00:00:00',
                'masatamatkeluar' => '00:00:00',
                'tujuan_id' => '1',
                'lainlain' => '',
                'gantian_id' => '1',
                'tarikhgantian' => '2019-09-11',
                'masamulagantian' => '00:00:00',
                'masatamatgantian' => '00:00:00',
                'status_permohonan_id' => '2',
                'sebab_ditolak' => '',
                'penyokong_user_role_id' => '1',
                'pelulus_user_role_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'user_id' => '1',
                'tarikhkeluar' => '2019-09-05',
                'masamulakeluar' => '00:00:00',
                'masatamatkeluar' => '00:00:00',
                'tujuan_id' => '1',
                'lainlain' => '',
                'gantian_id' => '1',
                'tarikhgantian' => '2019-09-11',
                'masamulagantian' => '00:00:00',
                'masatamatgantian' => '00:00:00',
                'status_permohonan_id' => '2',
                'sebab_ditolak' => '',
                'penyokong_user_role_id' => '1',
                'pelulus_user_role_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            
            [
                'user_id' => '1',
                'tarikhkeluar' => '2019-09-05',
                'masamulakeluar' => '00:00:00',
                'masatamatkeluar' => '00:00:00',
                'tujuan_id' => '1',
                'lainlain' => '',
                'gantian_id' => '1',
                'tarikhgantian' => '2019-09-11',
                'masamulagantian' => '00:00:00',
                'masatamatgantian' => '00:00:00',
                'status_permohonan_id' => '2',
                'sebab_ditolak' => '',
                'penyokong_user_role_id' => '1',
                'pelulus_user_role_id' => '2',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
          
             ]);
     
         }
    }
