<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('User')->truncate();
        DB::table('User')->insert(
        [
            [
            'nokp' => '1',
            'nama' => '1323456',
            'email' => 'user1@gmail.com',
            'notel' => '145',
            'avatar' => 'faf',
            'password' => bcrypt ('password'),
            'api_token' => hash('sha256', Str::random(60)),

            'status' => '1',
            //'api_token' => '1',
            'remember_token' =>null,
           'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
           'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
       ],
       [
        'nokp' => '2',
        'nama' => 'dua',
        'email' => 'user2@gmail.com',
        'notel' => '145',
        'avatar' => 'faf',
        'password' => bcrypt ('password'),
                    'api_token' => hash('sha256', Str::random(60)),
        'status' => '1',
        //'api_token' => '1',
        'remember_token' =>null,
       'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
       'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
    ], 
          
    ]);

}
}

       