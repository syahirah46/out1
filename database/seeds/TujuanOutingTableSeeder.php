<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TujuanOutingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  //
        DB::table('TujuanOuting')->truncate();
        DB::table('TujuanOuting')->insert(
        [
            [
                'nama' => 'Pemeriksaan kesihatan dengan temujanji/berkala',
               //  'seksyen_id' => '1',
                'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
                'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
            ],
            [
               'nama' => 'Lain-Lain',
               // 'seksyen_id' => '2',
               'created_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
               'updated_at'=> Carbon::now()->format ('Y-m-d H:i:s'),
           ], 
          
            ]);
    
        }
   }
