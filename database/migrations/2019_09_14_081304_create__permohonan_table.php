<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermohonanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Permohonan', function (Blueprint $table) {
            $table->increments('permohonan_id');
            $table->integer('user_id');
            $table->date('tarikhkeluar');
            $table->time('masamulakeluar');
            $table->time('masatamatkeluar');
            $table->integer('tujuan_id');
            $table->string('lainlain',191);
            $table->integer('gantian_id');
            $table->date('tarikhgantian');
            $table->time('masamulagantian');
            $table->time('masatamatgantian');
            $table->integer('status_permohonan_id');
            $table->string('sebab_ditolak',255);
            $table->integer('penyokong_user_role_id');
            $table->integer('pelulus_user_role_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Permohonan');
    }
}
