<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getApiUser($user_id)
    {
        $profil = \DB::table('User')
                ->select('User.user_id as userid', 'User.nokp', 'User.nama',
                'User.email', 'User.email', 'User.notel', 'User.avatar', 'User.password',
                'User.status', 
                'User_Perjawatan.jawatan_id as jawatanid', 'User_Perjawatan.bhgn_sek_id as bhgianseksyekid',
                'Bahagian_Seksyen.bahagian_id as bahagianid', 'Bahagian_Seksyen.seksyen_id',
                'Bahagian.nama as bahagianmana', 'Seksyen.nama as seksyenmana',
                'Jawatan.nama as jawatanapa','Jawatan.gred')
                ->leftJoin('User_Perjawatan', 'User_Perjawatan.user_id', '=', 'User.user_id')
                ->leftJoin('Jawatan', 'Jawatan.jawatan_id', '=', 'User_Perjawatan.jawatan_id')
                ->leftJoin('Bahagian_Seksyen', 'Bahagian_Seksyen.bhgn_sek_id', '=', 'User_Perjawatan.bhgn_sek_id')
                ->leftJoin('Bahagian', 'Bahagian.bahagian_id', '=', 'Bahagian_Seksyen.bahagian_id')
                ->leftJoin('Seksyen', 'Seksyen.seksyen_id', '=', 'Bahagian_Seksyen.seksyen_id')

                ->where('user.user_id', $user_id)
                ->get();

                return $profil;
    }

    public function getAlluser()
    {

        return User::get();
    }

    public function deactiveuser(Request $request, $user_id)
    {
        //        
        $user = User::find($user_id);
        $user->status = '0';
        $user->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        $user->save();

        return "User berhasil ubah";
    }

    public function activeuser(Request $request, $user_id)
    {
        //        
        $user = User::find($user_id);
        $user->status = '1';
        $user->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        $user->save();

        return "User berhasil ubah";
    }

    public function create(Request $request)
    {
        //
        $user = new User();
        $user->nokp = $request->nokp;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->notel = $request->notel;
        $user->avatar = $request->avatar;
        //$user->password = $request->password;
        $user->password =  bcrypt($request->password);
        $user->status = '1';
        $user->created_at = Carbon::now()->format ('Y-m-d H:i:s');
        $user->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        
        $user->save();

       

        return "Data berhasil masuk";
    }

    public function update(Request $request, $user_id)
    {
        //        
        $user = User::find($user_id);
        $user->notel = $request->notel;
        $user->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        $user->save();

        return "User berhasil ubah";
    }

    public function login_api(){
        
        
        $token = Str::random(60);
       
        $email = request()->get('email');
        $password = request()->get('password') ;
        $data = [];
        
        $header_code = 403;
        $success = false;
   
        if(Auth::attempt(['email' => $email, 'password' => $password])){

            $user = User::where('email',$email)->first();
             
            if(\Auth::user()->status == 1 ){
                 
                $user->forceFill([
                    'api_token' => hash('sha256',$token),
                    
                ])->save();
    
                $data['user'] = ['user_id' => $user->user_id, 'token' => $token];
    
                $header_code = 200;
                $success = true;
            }
           
        else{
            $header_code=401;
        }
    }
    $data['success'] = $success;
    return response($data, $header_code);
    }

    public function getUserAPi(){
        return \Auth::user();
    }
}
