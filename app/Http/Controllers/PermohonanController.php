<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permohonan;
use App\Gantian;
use App\User;
use App\TujuanOuting;


class PermohonanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getdashboardpengguna($user_id, $status_permohonan_id)
    {
        $permohon = Permohonan::where('status_permohonan_id',$status_permohonan_id)
                                ->where('user_id',$user_id)->get();    
        return $permohon;
    }

    public function totalpermohonan($user_id, $status_permohonan_id)
    {
        $mohonkira = Permohonan::select(\DB::raw('count(permohonan_id)'))
        ->where('user_id', $user_id)
        ->where('status_permohonan_id', $status_permohonan_id)
        ->get();
        return $mohonkira;
      
    }

    public function getpermohonanuser($permohonan_id)
    {
        // $permohon = Permohonan::where('id',$id)->get();    
        // return $permohon;
        
        $mohon = \DB::table('Permohonan')
                ->select('Permohonan.user_id', 'Permohonan.tarikhkeluar', 'Permohonan.masamulakeluar',
                'Permohonan.masatamatkeluar', 'Permohonan.tujuan_id','Permohonan.lainlain',
                'Permohonan.gantian_id','Permohonan.tarikhgantian','Permohonan.masamulagantian',
                'Permohonan.masatamatgantian', 'Permohonan.status_permohonan_id',
                'Permohonan.sebab_ditolak', 'Permohonan.penyokong_user_role_id',
                'Permohonan.pelulus_user_role_id',
                'User.user_id','User.nama as namauser', 'User.nokp',
                'TujuanOuting.nama as tujuanapa',
                'Gantian.nama as gantianapa',
                'User_Role.user_id as user_mana')
                // ->select('Gantian.nama as gantiannama')
                ->leftJoin('User', 'User.User_id', '=', 'Permohonan.user_id')
                ->leftJoin('Gantian', 'Gantian.gantian_id', '=', 'Permohonan.gantian_id')
                ->leftJoin('TujuanOuting', 'TujuanOuting.tujuan_id', '=', 'Permohonan.tujuan_id')
                ->leftJoin('User_Role', 'User_Role.user_role_id', '=', 'Permohonan.Penyokong_user_role_id')
                ->where('Permohonan.permohonan_id', $permohonan_id)
                ->get();

                return $mohon;
    }

    public function updatepermohonan(Request $request,$permohonan_id)
    {
        //     
        $permohonan = Permohonan::find($permohonan_id);
        $permohonan->user_id = $request->user_id;
        $permohonan->tarikhkeluar = $request->tarikhkeluar;
        $permohonan->masamulakeluar = $request->masamulakeluar;
        $permohonan->masatamatkeluar = $request->masatamatkeluar;
        $permohonan->tujuan_id = $request->tujuan_id;
        $permohonan->lainlain = $request->lainlain;
        $permohonan->gantian_id = $request->gantian_id;
        $permohonan->masamulagantian = $request->masamulagantian;
        $permohonan->masatamatgantian = $request->masatamatgantian;
        $permohonan->status_permohonan_id = '1';
        $permohonan->sebab_ditolak = $request->sebab_ditolak;
        $permohonan->penyokong_user_role_id = $request->penyokong_user_role_id;
        $permohonan->pelulus_user_role_id = $request->pelulus_user_role_id;
        $permohonan->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        $permohonan->save();

        return "mohon berhasil ubah";
    }

    public function create(request $request, $user_id){
        $permohonan = new Permohonan;
        $permohonan->user_id = $user_id;
        // $permohonan->updated_by = $user_id;
        $permohonan->tarikhkeluar = $request->tarikhkeluar;
        $permohonan->masamulakeluar = $request->masamulakeluar;
        $permohonan->masatamatkeluar = $request->masatamatkeluar;
        $permohonan->tujuan_id = $request->tujuan_id;
        $permohonan->lainlain = $request->lainlain;
        $permohonan->gantian_id = $request->gantian_id;
        $permohonan->status_permohonan_id = '0';
        $permohonan->tarikhgantian = $request->tarikhgantian;
        $permohonan->masamulagantian = $request->masamulagantian;
        $permohonan->masatamatgantian = $request->masatamatgantian;
        $permohonan->sebab_ditolak = '-';
        $permohonan->penyokong_user_role_id =  $request->penyokong_user_role_id;
        $permohonan->pelulus_user_role_id =  $request->pelulus_user_role_id;
        $permohonan->created_at = Carbon::now()->format ('Y-m-d H:i:s');
        $permohonan->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        $permohonan->save();

        return "Data berhasil masuk";
    }

    public function dashboardpegawai($seksyen_id, $status_permohonan_id)
    {
        // $permohon = Permohonan::where('status',$status)
        //                         ->where('user_id',)->get();    
        // return $permohon;

        $mohon = \DB::table('Permohonan')
                ->select('Permohonan.user_id', 'Permohonan.tarikhkeluar', 
                'User_Perjawatan.bhgn_sek_id',
                'Bahagian_Seksyen.bahagian_id as bahagian', 
                'Bahagian_Seksyen.seksyen_id as seksyen')
                ->leftJoin('User', 'User.user_id', '=', 'Permohonan.user_id')
                ->leftJoin('User_Perjawatan', 'User_Perjawatan.user_id', '=', 'Permohonan.user_id')
                ->leftJoin('Bahagian_Seksyen', 'Bahagian_Seksyen.bhgn_sek_id', '=', 'User_Perjawatan.bhgn_sek_id')
                ->where('Bahagian_Seksyen.seksyen_id', $seksyen_id)
                ->where('Permohonan.status_permohonan_id', $status_permohonan_id)
                ->get();

                return $mohon;
    }

    public function totalseksyen($seksyen_id, $status_permohonan_id)
    {
        $mohonkira = Permohonan::select(\DB::raw('count(permohonan_id)'))
        ->leftJoin('User', 'User.user_id', '=', 'Permohonan.user_id')
        ->leftJoin('User_Perjawatan', 'User_Perjawatan.user_id', '=', 'Permohonan.user_id')
        ->leftJoin('Bahagian_Seksyen', 'Bahagian_Seksyen.bhgn_sek_id', '=', 'User_Perjawatan.bhgn_sek_id')
        ->where('Bahagian_Seksyen.seksyen_id', $seksyen_id)
        ->where('Permohonan.status_permohonan_id', $status_permohonan_id)
        ->get();
        return $mohonkira;
      
    }

    public function pegawailulus(request $request, $permohonan_id){
        // $bahagian->nama = $request->nama;
        // $bahagian->updated_at = Carbon::now()->format ('Y-m-d H:i:s');


        $mohon = Permohonan::find($permohonan_id);
        $mohon->status = '1';
        $mohon->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        $mohon->save();

        return "Data berhasil ubah";
    }

    public function pegawaitaklulus(request $request, $permohonan_id){
        // $bahagian->nama = $request->nama;
        // $bahagian->updated_at = Carbon::now()->format ('Y-m-d H:i:s');


        $mohon = Permohonan::find($permohonan_id);
        $mohon->status = '2';
        $mohon->sebab_ditolak = $request->sebab_ditolak;
        $mohon->updated_at = Carbon::now()->format ('Y-m-d H:i:s');
        $mohon->save();

        return "Data berhasil ubah";
    }

    public function getbetween(){
       
        $from = date('2018-01-01');
        $to = date('2018-05-02');
        
        Reservation::whereBetween('reservation_from', [$from, $to])->get();

        
    }
}
