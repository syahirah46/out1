<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class User_RoleController extends Controller
{
    
    public function userpelulus($bahagian_id)
    {
        // $permohon = Permohonan::where('id',$id)->get();    
        // return $permohon;
        
        $pelulus = \DB::table('User_Role')
                ->select('User_Role.user_role_id', 'User_Role.role_id','User_Role.user_id',
                'User_Perjawatan.perjawatan_id',
                'User_Perjawatan.bhgn_sek_id',
                'Bahagian_Seksyen.bahagian_id',
                'Bahagian.nama as bahagianmana')
                ->leftJoin('User_Perjawatan', 'User_Perjawatan.user_id', '=', 'User_Role.user_id')
                ->leftJoin('Bahagian_Seksyen', 'Bahagian_Seksyen.bhgn_sek_id', '=', 'User_Perjawatan.bhgn_sek_id')
                ->leftJoin('Bahagian', 'Bahagian.bahagian_id', '=', 'Bahagian_Seksyen.bahagian_id')
                ->where('Bahagian.bahagian_id', $bahagian_id)
                ->where('User_Role.role_id', '3')
                ->get();

                return $pelulus;
    }

    public function userpenyokong($bahagian_id)
    {
        // $permohon = Permohonan::where('id',$id)->get();    
        // return $permohon;
        
        $pelulus = \DB::table('User_Role')
                ->select('User_Role.user_role_id', 'User_Role.role_id','User_Role.user_id',
                'User_Perjawatan.perjawatan_id',
                'User_Perjawatan.bhgn_sek_id',
                'Bahagian_Seksyen.bahagian_id',
                'Bahagian.nama as bahagianmana')
                ->leftJoin('User_Perjawatan', 'User_Perjawatan.user_id', '=', 'User_Role.user_id')
                ->leftJoin('Bahagian_Seksyen', 'Bahagian_Seksyen.bhgn_sek_id', '=', 'User_Perjawatan.bhgn_sek_id')
                ->leftJoin('Bahagian', 'Bahagian.bahagian_id', '=', 'Bahagian_Seksyen.bahagian_id')
                ->where('Bahagian.bahagian_id', $bahagian_id)
                ->where('User_Role.role_id', '2')
                ->get();

                return $pelulus;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
