<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ApiBasic
{
    public function handle($request, Closure $next)
    {

        if(!empty($request->header('PHP_AUTH_USER'))) {
            $api_token = $request->header('PHP_AUTH_USER');
        } else if(!empty($request->get('api_token'))) {
            $api_token = $request->get('api_token');
        } else {
            return response()->json(['error' => ['message' => "Missing Token", 'status_code' => '401']]);
        }

        $user = User::where('api_token', hash('sha256',$api_token))->first();

        if (empty($user))
        {
            $data = ['error' => ['message' => 'Invalid appkey', 'status_code' => 401]];
            return response()->json($data, 401);
        }

        auth()->setUser($user);

        return $next($request);
    }
}