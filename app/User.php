<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
//use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable
{
    use Notifiable;
   // use \OwenIt\Auditing\Auditable;

    protected $guarded = [];
    protected $table = 'user';

    protected $hidden = [
        'password', 'remember_token',
    ];


    // public function itemspecification() 
    // {
    //     return $this->hasMany(ItemSpecification::class);
    // }


// public function bahagian() 
// {
//     return $this->belongsTo('App\User', 'bahagian_id');
// }
}